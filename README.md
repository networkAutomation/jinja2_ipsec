`render.py`

Standard Python render for jinja2/YAML. Uses YAM file `yamlsVar`' as input variable forjinja2 template

### renderIpsecASA.py

Python render for jinja2 templatem for Cisco ASA IPSEC policy based. IKEv1 and IKEv2 supported
Usage: `python3 renders/renderIpsecASA.py templates/ipsecASA.j2 YAML/ipsecASA.yaml`

### renderIpsecJUNIPER.py

Python render for jinja2 template for SRX IPSEC route based. IKEv1 and IKEv2 supported
Usage: `python3 renders/renderIpsecJUNIPER.py templates/ipsecJUNIPER.j2 YAML/ipsecJUNIPER.yaml`

Output example are provided under `renders` folder
