#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
from netaddr import IPAddress
import yaml, sys

def main():

    ENV = Environment(loader=FileSystemLoader('.'))
    template = ENV.get_template(sys.argv[1])

    finalConf = []
    objGrDList = []
    objGrSList = []
    finalConf = []
    dstAddress = []

    with open(sys.argv[2], 'r') as f:
        yamlVar = yaml.load(f)

    for destinations in yamlVar[7]['destinations']:
        if destinations['ipDestination']['mask'] == '255.255.255.255':
            for ip in destinations['ipDestination']['address']:
                finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                objGrDList.append('H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                finalConf.append('  network-object host {}'.format(ip))
                dstAddress.append(ip)
        else:
            for ip in destinations['ipDestination']['address']:
                finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                objGrDList.append('N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                finalConf.append('  network-object {} {}'.format(ip, destinations['ipDestination']['mask']))
                dstAddress.append(ip)

        # Create source object-group
        for sources in yamlVar[6]['sources']:
            for ip in sources['ipSource']['address']:
                if sources['ipSource']['mask'] == '255.255.255.255':
                    finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                    objGrSList.append('H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                    finalConf.append('  network-object host {}'.format(ip))
                else:
                    finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                    objGrSList.append('N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                    finalConf.append('  network-object {} {}'.format(ip, sources['ipSource']['mask']))

        # Create destination object-group network
        finalConf.append('object-group network DG-{}'.format(yamlVar[0]['SNOW']['change']))
        DGLite = ('DG-{}'.format(yamlVar[0]['SNOW']['change']))
        for i in objGrDList:
            finalConf.append('  group-object {}'.format(i))
        objGrDList.clear()

        # Create source object-group network
        finalConf.append('object-group network SG-{}'.format(yamlVar[0]['SNOW']['change']))
        SGLite = ('SG-{}'.format(yamlVar[0]['SNOW']['change']))
        for i in objGrSList:
            finalConf.append('  group-object {}'.format(i))
        objGrSList.clear()

        # Create interface ACLs
        for key in yamlVar:
            if 'protPortDestination' in key:
                if key['protPortDestination']['protocol'] == 'tcp':
                    for i in key['protPortDestination']['ports']:
                        finalConf.append('access-list transit-in extended permit tcp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,i))
                elif key['protPortDestination']['protocol'] == 'udp':
                    for j in key['protPortDestination']['ports']:
                        finalConf.append('access-list transit-in extended permit udp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,j))
    finalConf.append('access-list {} extended permit ip object-group {} object-group {} log'.format(yamlVar[5]['splitTunnel']['aclName'],SGLite,DGLite))


    # config print
    for config in finalConf:
        print(config)

    print(template.render(yamlVar=yamlVar))

if __name__ == "__main__":
    main()
    '''Ver 1.1 f180516'''
