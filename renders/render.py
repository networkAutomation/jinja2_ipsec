#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
import  sys, os, yaml


def main():
    ENV = Environment(loader=FileSystemLoader('.'))
    template = ENV.get_template(sys.argv[1])

    with open(sys.argv[2], 'r') as yamlFile:
        yamlVar = yaml.load(yamlFile)
    print(template.render(yamlVar=yamlVar))

if __name__ == "__main__":
    main()
